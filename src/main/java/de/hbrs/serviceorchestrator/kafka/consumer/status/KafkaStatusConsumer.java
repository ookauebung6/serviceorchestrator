package de.hbrs.serviceorchestrator.kafka.consumer.status;

import de.hbrs.serviceorchestrator.AnalyseService;
import de.hbrs.serviceorchestrator.kafka.consumer.status.KafkaStatusDeserialiser;
import de.hbrs.wirschiffendas.data.entity.TransferItems.StatusTransferItem;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.common.errors.WakeupException;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.EnableAsync;

import java.util.List;
import java.util.Properties;

@EnableAsync
public class KafkaStatusConsumer implements Runnable {
    private AnalyseService analyseService;

    private Properties kafkaStatusProperties = new Properties();
    private static final String statusTopic = "ooka_hauserweber_status";
    private KafkaConsumer<String, StatusTransferItem> statusConsumer;



    public KafkaStatusConsumer() {
        kafkaStatusProperties.put("bootstrap.servers", "sepp-kafka.inf.h-brs.de:9092");
        kafkaStatusProperties.put("group.id", "de.hbrs.wirtschiffendas");
        kafkaStatusProperties.put("key.deserializer", StringDeserializer.class.getName());
        kafkaStatusProperties.put("value.deserializer", KafkaStatusDeserialiser.class.getName());

        statusConsumer = new KafkaConsumer<>(kafkaStatusProperties);
        statusConsumer.subscribe(List.of(statusTopic));
    }

    @Async
    public void listen() {
        try {
            while (true) {
                final ConsumerRecords<String, StatusTransferItem> consumerRecords = statusConsumer.poll(1000);
                consumerRecords.forEach(r -> {
                    StatusTransferItem statusTransferItem = r.value();
                    analyseService.sendResponseToUI(statusTransferItem.getAlgorithmIdentifier(), statusTransferItem.getStatus());
                });
            }
        } catch (WakeupException e) {
            // ignore
        } finally {
            statusConsumer.close();
        }
    }

    @Override
    public void run() {
        listen();
    }
}
