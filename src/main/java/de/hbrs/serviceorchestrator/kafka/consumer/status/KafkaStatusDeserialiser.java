package de.hbrs.serviceorchestrator.kafka.consumer.status;

import com.fasterxml.jackson.databind.ObjectMapper;
import de.hbrs.wirschiffendas.data.entity.TransferItems.StatusTransferItem;
import org.apache.kafka.common.serialization.Deserializer;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.EnableAsync;

import java.nio.charset.StandardCharsets;
import java.util.Map;

@EnableAsync
public class KafkaStatusDeserialiser implements Deserializer<StatusTransferItem> {

    private ObjectMapper objectMapper = new ObjectMapper();

    @Override
    public void configure(Map<String, ?> configs, boolean isKey) {
    }

    @Async
    @Override
    public StatusTransferItem deserialize(String s, byte[] bytes) {
        try {
            return objectMapper.readValue(new String(bytes, StandardCharsets.UTF_8), StatusTransferItem.class);
        } catch (Exception e) {
            System.err.println("Status Transfer Item konnte nicht deserialisiert werden.");
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public void close() {
    }
}
