package de.hbrs.serviceorchestrator.kafka.consumer.analysis;

import com.fasterxml.jackson.databind.ObjectMapper;
import de.hbrs.wirschiffendas.data.entity.TransferItems.AnalysisTransferItem;
import org.apache.kafka.common.serialization.Deserializer;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.EnableAsync;

import java.nio.charset.StandardCharsets;
import java.util.Map;

@EnableAsync
public class KafkaAnalysisDeserialiser implements Deserializer<AnalysisTransferItem> {

    private ObjectMapper objectMapper = new ObjectMapper();

    @Async
    @Override
    public AnalysisTransferItem deserialize(String s, byte[] bytes) {
        try {
            return objectMapper.readValue(new String(bytes, StandardCharsets.UTF_8), AnalysisTransferItem.class);
        } catch (Exception e) {
            System.err.println("Analysis Transfer Item konnte nicht deserialisiert werden.");
            e.printStackTrace();
            return null;
        }
    }
}
