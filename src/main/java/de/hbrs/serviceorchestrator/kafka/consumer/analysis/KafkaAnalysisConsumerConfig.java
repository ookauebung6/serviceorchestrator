package de.hbrs.serviceorchestrator.kafka.consumer.analysis;

import de.hbrs.wirschiffendas.data.entity.TransferItems.AnalysisTransferItem;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.annotation.EnableKafka;
import org.springframework.kafka.config.ConcurrentKafkaListenerContainerFactory;
import org.springframework.kafka.core.ConsumerFactory;
import org.springframework.kafka.core.DefaultKafkaConsumerFactory;

import java.util.HashMap;
import java.util.Map;

@EnableKafka
@Configuration
public class KafkaAnalysisConsumerConfig {

    @Bean
    public ConsumerFactory<String, AnalysisTransferItem> consumerFactory() {
        Map<String, Object> props = new HashMap<>();
        props.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, "sepp-kafka.inf.h-brs.de:9092");
        props.put(ConsumerConfig.GROUP_ID_CONFIG, "ookahauserweber.wirschiffendas.orch_cons");
        props.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class);
        props.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, KafkaAnalysisDeserialiser.class);
        return new DefaultKafkaConsumerFactory<>(props);
    }

    @Bean
    public ConcurrentKafkaListenerContainerFactory<String, AnalysisTransferItem> kafkaListenerContainerFactory() {
        ConcurrentKafkaListenerContainerFactory<String, AnalysisTransferItem> factory = new ConcurrentKafkaListenerContainerFactory<>();
        factory.setConsumerFactory(consumerFactory());
        return factory;
    }
}
