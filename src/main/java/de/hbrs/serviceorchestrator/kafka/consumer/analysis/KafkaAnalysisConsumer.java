package de.hbrs.serviceorchestrator.kafka.consumer.analysis;

import de.hbrs.serviceorchestrator.AnalyseService;
import de.hbrs.wirschiffendas.data.entity.TransferItems.AnalysisTransferItem;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;

@Component
public class KafkaAnalysisConsumer {

    //TODO: Autowired?

    @KafkaListener(topics = "ooka_hauserweber_analysis", groupId = "ookahauserweber.wirschiffendas.orch_cons")
    public void listenAnalysis (AnalysisTransferItem analysisTransferItem) {
        System.out.println("Kafka: Message eingegangen");
        new AnalyseService().analyse(analysisTransferItem);
    }
}
