package de.hbrs.serviceorchestrator;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import de.hbrs.serviceorchestrator.producer.configuration.KafkaConfigurationProducer;
import de.hbrs.wirschiffendas.data.entity.AlgorithmIdentifier;
import de.hbrs.wirschiffendas.data.entity.Configuration;
import de.hbrs.wirschiffendas.data.entity.Status;
import de.hbrs.wirschiffendas.data.entity.TransferItems.AnalysisTransferItem;
import de.hbrs.wirschiffendas.data.entity.TransferItems.StatusTransferItem;
import io.github.resilience4j.circuitbreaker.annotation.CircuitBreaker;
import org.springframework.http.ResponseEntity;
import org.springframework.lang.Nullable;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import java.net.URI;
import java.net.URISyntaxException;

@RestController
@CircuitBreaker(name = "liquidCheck")
@EnableAsync
public class AnalyseService {
    /*
        Services im Orchester:
            Liquid --> 8082
            Mechanic --> 8083
            Software --> 8084
     */

    public AnalyseService() {
        //run();
    }


    //--------------
    //    KAFKA
    //--------------

    public void analyse (AnalysisTransferItem analysisTransferItem) {
        Configuration configuration = analysisTransferItem.getConfiguration();
        AlgorithmIdentifier algorithmIdentifier = analysisTransferItem.getAlgorithmIdentifier();

        runMicroservices(configuration, algorithmIdentifier);
    }

    public void callServiceWithKafka (Configuration configuration, AlgorithmIdentifier algorithmIdentifier) {
        String topic = null;
        switch (algorithmIdentifier) {
            case MECHANICAL -> topic = "ooka_hauserweber_configuraton_mechanical";
            case SOFTWARE -> topic = "ooka_hauserweber_configuraton_software";
            case LIQUID -> topic = "ooka_hauserweber_configuraton_liquid";
        }
        if (topic == null) {
            this.sendErrorStatusResponse(algorithmIdentifier);
        }
        try {
            new KafkaConfigurationProducer().sendConfiguration(configuration, topic);
        } catch (Exception e) {
            this.sendErrorStatusResponse(algorithmIdentifier);
        }
    }

    //--------------
    //  END KAFKA
    //--------------

    @PostMapping(value = "/analyse", consumes = "application/json")
    public void analyse(@RequestBody String json) {
        Configuration conf = null;
        try {
            conf = new ObjectMapper().readValue(json, Configuration.class);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }

        runMicroservices(conf, null);
    }

    @PostMapping(value = "/analyse/{id}", consumes = "application/json")
    public void analyseSpecific(@RequestBody String json, @PathVariable String id) {
        try {
            Configuration configuration = new ObjectMapper().readValue(json, Configuration.class);
            AlgorithmIdentifier algorithmIdentifier;
            switch (id.toLowerCase()) {
                case "liquid" -> algorithmIdentifier = AlgorithmIdentifier.LIQUID;
                case "software" -> algorithmIdentifier = AlgorithmIdentifier.SOFTWARE;
                case "mechanical" -> algorithmIdentifier = AlgorithmIdentifier.MECHANICAL;
                default -> throw new NullPointerException();
            }
            runMicroservices(configuration, algorithmIdentifier);
        } catch (JsonProcessingException | NullPointerException e) {
            e.printStackTrace();
        }
    }

    public void runMicroservices(Configuration configuration, @Nullable AlgorithmIdentifier identifier) {
        if (identifier == null) {
            callLiquidCheck(configuration);
            callMechanicalCheck(configuration);
            callSoftwareCheck(configuration);
        } else {
            switch (identifier) {
                case LIQUID -> callLiquidCheck(configuration);
                case SOFTWARE -> callSoftwareCheck(configuration);
                case MECHANICAL -> callMechanicalCheck(configuration);
            }
        }
    }

    @Async
    void callSoftwareCheck(Configuration configuration) {
        AlgorithmIdentifier algorithmIdentifier = AlgorithmIdentifier.SOFTWARE;
        try {
            callServiceWithKafka(configuration, algorithmIdentifier);
        } catch (Exception e) {
            e.printStackTrace();
            sendErrorStatusResponse(algorithmIdentifier);
        }
    }

    @Async
    void callMechanicalCheck(Configuration configuration) {
        AlgorithmIdentifier algorithmIdentifier = AlgorithmIdentifier.MECHANICAL;
        try {
            callServiceWithKafka(configuration, algorithmIdentifier);
        } catch (Exception e) {
            e.printStackTrace();
            sendErrorStatusResponse(algorithmIdentifier);
        }
    }

    @Async
    public void callLiquidCheck(Configuration configuration) {
        AlgorithmIdentifier algorithmIdentifier = AlgorithmIdentifier.LIQUID;
        try {
            callServiceWithKafka(configuration, algorithmIdentifier);
        } catch (Exception e) {
            e.printStackTrace();
            sendErrorStatusResponse(algorithmIdentifier);
        }
    }

    @Async
    public void callService(Configuration configuration, AlgorithmIdentifier ae) throws Exception {
        String url ;
        switch (ae) {
            case LIQUID -> url = "http://10.25.0.3:8082/checkConfig";
            case MECHANICAL -> url = "http://10.25.0.2:8083/checkConfig";
            case SOFTWARE -> url = "http://10.25.0.4:8084/checkConfig";
            default -> throw new Exception();
        }

        RestTemplate restTemplate = new RestTemplate();
        URI uri = new URI(url);
        ResponseEntity<Configuration> responseEntity = restTemplate.postForEntity(uri, configuration, Configuration.class);
        System.out.println(responseEntity.getStatusCode());
    }

    public void sendErrorStatusResponse(AlgorithmIdentifier algorithmIdentifier) {
        Status status = Status.FAILED;
        sendResponseToUI(algorithmIdentifier, status);
    }

    public void sendResponseToUI(AlgorithmIdentifier algorithmIdentifier, Status status) {
        StatusTransferItem statusTransferItem = new StatusTransferItem(algorithmIdentifier, status);
        String url = "http://localhost:8080/statusUpdate";

        RestTemplate restTemplate = new RestTemplate();
        System.out.println("Angekommen " + algorithmIdentifier + " " + status);
        try {
            URI uri = new URI(url);
            ResponseEntity<StatusTransferItem> responseEntity = restTemplate.postForEntity(uri, statusTransferItem, StatusTransferItem.class);
            System.out.println(responseEntity.getStatusCode());
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
    }
}
