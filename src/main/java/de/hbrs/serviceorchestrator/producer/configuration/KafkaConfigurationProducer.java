package de.hbrs.serviceorchestrator.producer.configuration;

import de.hbrs.wirschiffendas.data.entity.Configuration;
import de.hbrs.wirschiffendas.data.entity.TransferItems.AnalysisTransferItem;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Component;

@Component
public class KafkaConfigurationProducer {
    @Autowired
    private KafkaTemplate<String, Configuration> kafkaTemplate;

    public void sendConfiguration (Configuration configuration, String topic) throws Exception {
        new KafkaConfigurationProducerConfig().kafkaTemplate().send(topic, configuration);
    }
}
