package de.hbrs.serviceorchestrator.producer.configuration;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import de.hbrs.wirschiffendas.data.entity.Configuration;
import org.apache.kafka.common.serialization.Serializer;

public class KafkaConfigurationSerializer implements Serializer<Configuration> {

    private ObjectMapper objectMapper = new ObjectMapper();

    @Override
    public byte[] serialize(String s, Configuration configuration) {
        try {
            return objectMapper.writeValueAsBytes(configuration);
        } catch (JsonProcessingException e) {
            System.err.println("Objekt konnte nicht serialisiert werden!");
            e.printStackTrace();
            return null;
        }
    }
}
